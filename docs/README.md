# PINGER (Go Application) Containerization and CI/CD Pipeline

Pinger is application, written in GoLang programming language. This service reponds with `"hello world"` at the root path and can be configured to ping another server through the environment variables (see the file at `./cmd/pinger/config.go`). By default, running it will make it start a server and ping itself. 

A basic `Makefile` is provided that allows you to:

- pull in dependencies - `make dep`
- builds the binaries - `make build`
- test runs - `make run`
- run tests - `make test`


## Pre-requisites

You will need the following installed:

- `go` to run the application (check with `go version`)
- `docker` for image building/publishing (check with `docker version`)
- `docker-compose` for environment provisioning (check with `docker-compose version`)
- `git` for source control (check with `git -v`)
- `make` for simple convenience scripts (check with `make -v`)

You will also need the following accounts:

- GitLab.com ([click here to register/login](https://gitlab.com/users/sign_in))

## Directory structure

| Directory | Description |
| --- | --- |
| `/bin` | Contains binaries |
| `/cmd` | Contains source code for CLI interfaces |
| `/deployments` | Contains manifests for deployments and docker-compose file for container workflow deployment |
| `/deployments/build` | Contains docker file |
| `/docs` | Contains documentation |
| `/vendor` | Contains dependencies (use `make dep` to populate it) |
| `/build` | Contains docker image artifact tarball |


## Containerization
[Dockerfile](https://gitlab.com/hninthidawin/devops-checkin/-/blob/master/deployments/build/Dockerfile) can be found under /deployments/build directory as below:

```dockerfile
# syntax=docker/dockerfile:1

FROM golang:1.16-alpine

WORKDIR /app

COPY go.mod ./ 
COPY go.sum ./

RUN go mod download

COPY /cmd/pinger/*.go ./

RUN go build -o main .
#RUN make dep && make build && make run

EXPOSE 8000

CMD [ "/app/main" ]
```

#### Generate container image from Dockerfile
```
docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest .
```
The successful docker build process will result in image named called `devops/pinger:latest`. 

You can find container images list in your environment via the command `docker image ls`.
#### Create Containers from image (devops/pinger:latest)
```
docker run -it -p 8000:8000 devops/pinger:latest
```

## CI/CD Pipeline
You may update the CI pipeline by changing [`.gitlab-ci.yml`](https://gitlab.com/hninthidawin/devops-checkin/-/blob/master/.gitlab-ci.yml) under root directory. Once the new code is committed and pushed into gitlab, gitlab will start running the new pipeline under CI/CD session in left navigator. 

`.gitlab-ci.yml` is written in yml format and it consists of 3 stages (build, test and release). 

#### Build
Building process shall install dependencies and build the binary. Output binary named `pinger` are stored as artifact under /bin directory. You may download it any time from GitLab portal -> CI/CD pipeline.

#### Test
Test process shall run `make test` command to make sure our GO application `pinger` without errors before releasing image file.

#### Release
Release process shall generate the container images via `make docker-image` command. The output of container images are archived via `make docker_tar` command and stored as artifact under /build directory.

### [GitLab Runner](https://docs.gitlab.com/runner/)
GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline.

You can choose to use Runner on GitLab or install the GitLab Runner application on infrastructure that you own or manage. In this set up, one GitLab Runner is installed locally on VM. You can find detail on [`How to install GitLab Runner`](https://gitlab.com/help/ci/runners/index)

## Workflow (docker-compose)
[`docker-compose.yml`](https://gitlab.com/hninthidawin/devops-checkin/-/blob/master/deployments/docker-compose.yml) is a workflow, which can generate multi-containers Docker applications. `docker-compose.yml` file is created under /deployments directory. 

current docker-compose.yml workflow used pre-populated image `devops/pinger:latest` and inject the environment variables, such as TARGET_HOST, TARGET_PORT and PORT to complete the 2 containers ping process.

#### Usage
Run the workflow and provision a chain of containers.
```
docker-compose -f ./deployments/docker-compose.yml up
```

Stop the workflow and delete a chain of containers.
```
docker-compose -f ./deployments/docker-compose.yml down
```

You may run the workflow with multiple containers at background by adding option -d

## Cleanup
It's good practice to clean up the unnecessary resources in your environment. 
#### Delete unused container
```
docker rm <CONTAINER_ID>
```
#### Delete unused container image
```
docker rmi <CONTAINER_IMAGE_ID>
```
#### docker-compose workflow deletion
```
docker-compose -f ./deployments/docker-compose.yml down
```
